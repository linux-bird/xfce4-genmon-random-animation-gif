# Xfce4-panel genmonitor goodie. 

## Show random gif animations with genmon.

-- Keep icons folder and *.sh in the same folder. 

-- Folder Org-Gifs, says what it is. Original Size.

-- Folder resize gifs. A script to convert/resize a folder with gifs. 

---

>
# Change size of multiple gif files from folder :: Deps ##

###### dep :: imagemagick

`$ sudo pacman -S imagemagick`

`$ sudo apt install imagemagick`

###### Optional dep :: pv

`$ sudo apt install pv`

` $ yay -S pv`
>

----------------------------------
# Resize Gif Folder :: How .......
----------------------------------

- Extract the archive to a folder.

- Make script executable. 

`$ chmod +x resize-gif-folder.sh`


- Make a copy of the original gif files and put them inside Convert folder.

- Check somehow (inkscape) the original size of the gifs (they all have to be the same, more or less). 

- Write this down. ORG

- Check the final size you want to achive.

- Write this down. FINAL


## Run script.

`$ ./resize-gif-folder.sh`

- 1e Folder = Convert

- 2e ORG size = 000x000

- 3e FINAL size = 000x000


:: Error-output :: you get an Argument ::

:)

