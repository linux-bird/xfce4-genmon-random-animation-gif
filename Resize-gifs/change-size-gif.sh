#!/bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
WHITE='\033[0;37m'
RESET='\033[0m'

prog=$(basename $0)

read -p "Enter existing directory : " input

read -p "Enter Original Size : " orgsz

read -p "Enter Final Size : " finalsz


error_out() { echo "$input" 1>&2; exit 1; }

echo  ""

[ $input ] || error_out "   1e--->  Argument, must be an existing directory" :\

echo ""

[ $orgsz ] || error_out "   2e -  Argument, must be Input Original Size  000x000" 

echo ""

[ $finalsz ] || error_out "   3e -  Argument, must be Input Final Size 000x000"
 
echo ""

gifs=$(find $input -type f -iname '*.gif')


for p in $gifs; do
    echo "$p"   
	
	 
	 convert -size "$orgsz" $p -resize "$finalsz" $p  | pv  -t
	 echo ""
done

